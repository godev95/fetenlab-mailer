<?php namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Controllers\BaseController;



Class MailController{


    public function sendEmail($request,$response,$arg){    

        $jsonmail = file_get_contents(__DIR__."../../../environment.json");
        $configmail = json_decode($jsonmail,true);


        $mail = new PHPMailer(true);  
        $valueBody = json_decode($request->getBody());

        if (isset($valueBody->name)) {
            $name = $valueBody->name;
        }else{
                 
            $arrayResponse = array(
                "state" => "success",
                "msg" => "Name is required",
                "data" => null
            );
              //enviar la respuesta
            $response->getBody()->write(json_encode($arrayResponse));
            return $response
            ->withHeader('Content-Type','application/json')
            ->withStatus(400);

        }

        if (isset($valueBody->email)) {
            $email = $valueBody->email;
        }else{
            $arrayResponse = array(
                "state" => "success",
                "msg" => "Email is required",
                "data" => null
            );
              //enviar la respuesta
            $response->getBody()->write(json_encode($arrayResponse));
            return $response
            ->withHeader('Content-Type','application/json')
            ->withStatus(400);
        }

        if (isset($valueBody->message)) {
            $message = $valueBody->message;
        }else{
            $arrayResponse = array(
                "state" => "success",
                "msg" => "Message is required",
                "data" => null
            );
              //enviar la respuesta
            $response->getBody()->write(json_encode($arrayResponse));
            return $response
            ->withHeader('Content-Type','application/json')
            ->withStatus(400);
        }

        if (isset($valueBody->phone)) {
            $phone = $valueBody->phone;
        }else{
            $phone = "null";
        }

        try{

             //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $configmail["EMAIL_USERNAME"];                 // SMTP username (email sender)
            $mail->Password = $configmail["EMAIL_PASSWORD"];                 // SMTP password
            $mail->SMTPSecure = 'tls';            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                    // TCP port to connect to

            //Recipients
            $mail->setFrom($configmail["EMAIL_USERNAME"], 'Fetenlab');
            $mail->addAddress("fetenlab.dev@gmail.com", 'Cliente');     // Add a recipient
            // $mail->addAddress('ellen@example.com');               // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

             //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Solicitud de servicios para el cliente';
            $mail->Body    = "nombre:".$name." email:".$email." telefono:".
            $phone." 
            Mensaje:
            ".$message;
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();

            $arrayResponse = array(
                "state" => "success",
                "msg" => "Email sent successfully",
                "data" => null
            );
              //enviar la respuesta
            $response->getBody()->write(json_encode($arrayResponse));
            return $response
            ->withHeader('Content-Type','application/json')
            ->withStatus(200);



        }catch(Exception $ex){

            $arrayResponse = array(
                "state" => "error",
                "msg" => "Server error",
                "data" => null
              );
               
            $response->getBody()->write($arrayResponse);
            return $response
            ->withHeader('Content-Type','application/json')
            ->withStatus(500);


        }

    }
}