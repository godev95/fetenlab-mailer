<?php

//nos permite utilizar nuestras rutas en grupo
use Slim\Routing\RouteCollectorProxy;

/*
$app->group('/api', function(RouteCollectorProxy $group){
     $group->get('/test','App\Controllers\TestController:getAll');
}); */

$app->group('/api', function(RouteCollectorProxy $group){
    $group->post('/sendEmail','App\Controllers\MailController:sendEmail');
});

