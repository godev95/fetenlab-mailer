<?php



$container->set('db_settings',function(){

  
  $jsonbd = file_get_contents(__DIR__."../../../environment.json");
  $confibd = json_decode($jsonbd,true);

   return (object)[
     "DB_NAME" => $confibd["DB_NAME"],
     "DB_PASS" => $confibd["DB_PASS"],
     "DB_CHAR" => $confibd["DB_CHAR"],
     "DB_HOST" => $confibd["DB_HOST"],
     "DB_USER" => $confibd["DB_USER"],
   ];
});

